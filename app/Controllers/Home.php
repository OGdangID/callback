<?php

namespace App\Controllers;

use Xendit\Xendit;
use chillerlan\QRCode\{QRCode, QROptions};

Xendit::setApiKey('xnd_development_cgzMTWeqQKt2u2zkj62OqYwem2BeRz9HDVcOQKiQ15WEaXlWzcbCulJaIKGs');

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	public function saldo()
	{
		$getBalance = \Xendit\Balance::getBalance('CASH');
		echo json_encode($getBalance);
	}

	public function qris()
	{
		$params = [
			'external_id' => 'external_6',
			'type' => 'STATIC',
			'callback_url' => 'https://webhook.site',
			'amount' => 10000,
		];

		$created_qr_code = \Xendit\QRCode::create($params);
		$gzip = true;

		$options = new QROptions([
			'version'      => QRCode::VERSION_AUTO,
			'outputType'   => QRCode::OUTPUT_MARKUP_SVG,
			'imageBase64'  => false,
			'eccLevel'     => QRCode::ECC_L,
			'svgViewBoxSize' => 530,
			'addQuietzone' => true,
			'cssClass'     => 'my-css-class',
			'svgOpacity'   => 1.0,
			'svgDefs'      => '
		<linearGradient id="g2">
			<stop offset="0%" stop-color="#39F" />
			<stop offset="100%" stop-color="#F3F" />
		</linearGradient>
		<linearGradient id="g1">
			<stop offset="0%" stop-color="#F3F" />
			<stop offset="100%" stop-color="#39F" />
		</linearGradient>
		<style>rect{shape-rendering:crispEdges}</style>',
			'moduleValues' => [
				// finder
				1536 => 'url(#g1)', // dark (true)
				6    => '#fff', // light (false)
				// alignment
				2560 => 'url(#g1)',
				10   => '#fff',
				// timing
				3072 => 'url(#g1)',
				12   => '#fff',
				// format
				3584 => 'url(#g1)',
				14   => '#fff',
				// version
				4096 => 'url(#g1)',
				16   => '#fff',
				// data
				1024 => 'url(#g2)',
				4    => '#fff',
				// darkmodule
				512  => 'url(#g1)',
				// separator
				8    => '#fff',
				// quietzone
				18   => '#fff',
			],
		]);

		$qrcode = (new QRCode($options))->render($created_qr_code['qr_string']);

		header('Content-type: image/svg+xml');

		if ($gzip === true) {
			header('Vary: Accept-Encoding');
			header('Content-Encoding: gzip');
			$qrcode = gzencode($qrcode, 9);
		}
		echo $qrcode;
		// dd($created_qr_code);
	}
}
