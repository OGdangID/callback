<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Api extends ResourceController
{
    protected $modelName = 'App\Models\Api';
    protected $format    = 'json';


    public function index()
    {
        $token = $this->request->getHeaderLine('x-callback-token');
        if ($token == '4ce3229c26004e38b0be19100a4c6f7c8a8eb0c3d7cb7a7dc0bebce5c7525870') {
            $data = [
                'status' =>  200,
                'data' => $this->request->getPost(),
                'message' => 'success',
            ];
        } else {
            $data = [
                'status' =>  440,
                'data' => $this->request->getPost(),
                'message' => 'akses di tutup',
            ];
        }
        return $this->respond($data);
        // return $this->respond($this->model->findAll());s
    }
}
